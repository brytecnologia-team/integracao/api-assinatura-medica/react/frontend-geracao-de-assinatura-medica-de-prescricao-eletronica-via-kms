import React, { useState } from "react";
import { validaCPF } from '../../utils/utils.js'
import axios from 'axios';

export default function Kms() {

  // CRIA TODAS AS VARIAVEIS QUE SERÃO ENVIADAS NA REQUISIÇÃO
  const [tipo_credencial, setTipoCredencial] = useState("PIN");
  const [valor_credencial, setValorCredencial] = useState("");
  const [cpf, setCpf] = useState("");
  const [perfil, setPerfil] = useState("BASICA");
  const [algoritmoHash, setAlgoritmoHash] = useState("SHA1");
  const [documento, setDocumento] = useState("");
  const [loading, setLoading] = useState(false);
  const [tipoDocumento, setTipoDocumento] = useState("");
  const [tipoProfissional, setTipoProfissional] = useState("");
  const [especialidade, setEspecialidade] = useState("");
  const [numero, setNumero] = useState("");
  const [UF, setUF] = useState("");
  var   [especialidadeOID] = useState("");
  var   [numeroOID] = useState("");
  var   [UFOID] = useState("");
  const [assinaturaVisivel, setAssinaturaVisivel] = useState("false");
  const [incluirIMG, setIncluirIMG] = useState("false");
  const [incluirTXT, setIncluirTXT] = useState("false");
  const [imagem, setImagem] = useState("");
  const [altura, setAltura] = useState("");
  const [largura, setLargura] = useState("");
  const [coordenadaX, setCoordenadaX] = useState("");
  const [coordenadaY, setCoordenadaY] = useState("");
  const [posicao, setPosicao] = useState("INFERIOR_ESQUERDO");
  const [pagina, setPagina] = useState("PRIMEIRA");
  const [texto, setTexto] = useState("");
  const [incluirCN, setIncluirCN] = useState(false);
  const [incluirCPF, setIncluirCPF] = useState(false);
  const [incluirEmail, setIncluirEmail] = useState(false);
  const [cpfInvalido, setCpfInvalido] = useState(false);


  // FUNCÃO QUE SERÁ EXECUTADA QUANDO FOR CLICADO NO BOTÃO "Assinar"
    async function handleSubmit(event) {

        // PREVINE AÇÕES PADÕRES DE UMA PAGINA REACT
        event.preventDefault();
        // UTILIZA A FUNCÃO "validaCPF" DE UTILS PARA VALIDAR O CPF DO INPUT 
        if (cpf === "" || validaCPF(cpf)) {
            setCpfInvalido(false);
        } else {
            setCpfInvalido(true);
            return;
        }
        // ALTERA A VARIÁVEL LOADING PRA TRUE PARA QUE APAREÇA MENSAGEM DE "Realizando assinatura do documento"
        setLoading(true);
        // CRIA O FORMDATA QUE SERÁ ENVIADO NA REQUISIÇÃO
        const data = new FormData();
        // INCLUIR AS VARIÁVEIS NO FORMDATA
        // CREDENCIAIS DEFINIDAS
        data.append("tipo_credencial", tipo_credencial);
        if (tipo_credencial === "PIN") {
            // CREDENCIAIS DO TIPO PIN DEVEM ESTAR CODIFICADAS EM BASE64
            data.append("valor_credencial", btoa(valor_credencial));
        } else {
            data.append("valor_credencial", valor_credencial);
        }
        // CPF DO ASSINANTE
        if (!(cpf === "")) {
            data.append("signatario", cpf);
        }
        // PERFIL DE ASSINATURA (BASICA OU CARIMBO)
        data.append("perfil", perfil);
        // ALGORITMO HASH QUE SERÁ USADO NA CODIFICAÇÃO DO DOCUMENTO
        data.append("algoritmoHash", algoritmoHash);
        // DOCUMENTO NO FORMATO PDF QUE SERÁ ASSINADO
        data.append("documento", documento);
        // SE A ASSINATURA SERÁ VISIVEL NO DOCUMENTO
        data.append("assinaturaVisivel", assinaturaVisivel)

        if (assinaturaVisivel === "true") {
            // ALTURA DO CAMPO DE ASSINATURA
            data.append("altura", altura);
            // LARGURA DO CAMPO DE ASSINATURA
            data.append("largura", largura);
            // COORDENADA NO EIXO X ONDE O CAMPO DE ASSINATURA SERÁ POSICIONADO
            data.append("coordenadaX", coordenadaX);
            // COORDENADA NO EIXO Y ONDE O CAMPO DE ASSINATURA SERÁ POSICIONADO
            data.append("coordenadaY", coordenadaY);
            // POSIÇÃO DO CAMPO DE ASSINATURA (INFERIOR_ESQUERDO, INFERIOR_DIREITO, SUPERIOR_DIREITO, SUPERIOR_ESQUERDO)
            data.append("posicao", posicao);
            // PAGINA DA ASSINATURA
            data.append("pagina", pagina);
            // SE SERÁ INCLUSO OU NÃO O TEXTO
            data.append("incluirTXT", incluirTXT);

            if (incluirTXT === "true") {
                // TEXTO DE ASSINATURA
                data.append("texto", texto);
            }
            // SE SERÁ INCLUSO OU NÃO A IMAGEM
            data.append("incluirIMG", incluirIMG);

            if (incluirIMG === "true") {
                // IMAGEM QUE SERÁ INSERIDA NA ASSINATURA. ACEITA PNG, JPEG E BMP.
                data.append("imagem", imagem);
            }
            // SE SERÁ INCLUSO CPF NA ASSINATURA
            data.append("incluirCPF", incluirCPF);
            // SE SERÁ INCLUSO O NOME DO ASSINANTE NA ASSINATURA (COMMON NAME)
            data.append("incluirCN", incluirCN);
            // SE SERÁ INCLUSO O EMAIL DO ASSINANTE NA ASSINATURA
            data.append("incluirEmail", incluirEmail);
        }

        // SE SERÁ INCLUSO OU NÃO A IMAGEM
        data.append("incluirIMG", incluirIMG);
        // SE SERÁ INCLUSO OU NÃO O TEXTO
        data.append("incluirTXT", incluirTXT);


        //ATRIBUI OS OIDs RESPECTIVOS AO TIPO DE MÉDICO
        if (tipoProfissional === "MEDICO") {
            numeroOID = "2.16.76.1.4.2.2.1";
            UFOID = "2.16.76.1.4.2.2.2";
            especialidadeOID = "2.16.76.1.4.2.2.3";
        } else if (tipoProfissional === "FARMACEUTICO") {
            numeroOID = "2.16.76.1.4.2.3.1";
            UFOID = "2.16.76.1.4.2.3.2";
            especialidadeOID = "2.16.76.1.4.2.3.3";
        } else {
            numeroOID = "2.16.76.1.4.2.12.1";
            UFOID = "2.16.76.1.4.2.12.2";
            especialidadeOID = "2.16.76.1.4.2.12.2";
        }
        data.append("tipoDocumento", tipoDocumento);
        data.append("tipoProfissional", tipoProfissional);
        data.append("numero", numero);
        data.append("numeroOID", numeroOID);
        data.append("UF", UF);
        data.append("UFOID", UFOID);
        data.append("especialidade", especialidade);
        data.append("especialidadeOID", especialidadeOID);

        if (tipoProfissional === '' || tipoDocumento === '' || numero == '' || UF == '' || especialidade == '') {
            window.alert("Favor conferir os dados preenchidos")
            console.log("Favor conferir os dados preenchidos")
            setLoading(false);
        } else {
            try {
                // FAZ A REQUISIÇÃO DE ASSINATURA PARA O BACKEND
                const response = await axios.post("/assinador/assinarKMS", data);
                // FAZ DOWNLOAD DO ARQUIVO COM A HREF QUE VEM NA RESPOSTA DA REQUISIÇÃO
                console.log(response.data);
                window.location.href = response.data;
            } catch (err) {
                // CASO OCORRA ALGUM ERRO NA REQUISIÇÃO, MOSTRA UMA MENSAGEM PARA O USUÁRIO
                console.log(err);
                try {
                  window.alert("Erro ao assinar o documento: " + JSON.parse(err.response.data).message);
                } catch (errAlert) {
                  window.alert("Erro ao assinar documento: " + err.response.data.message);
                }
            }
            setLoading(false);
        }
    }

  // ATUALIZA O TIPO DA CREDENCIAL UTILIZADO. SE CREDENCIAL TIPO DA CREDENCIAL FOR DE OU PARA PIN, RESETA O CAMPO
  function setTipoCredencialFunc (value) {
    if (value === "PIN" || tipo_credencial === "PIN") {
      setValorCredencial("");
    }
    setTipoCredencial(value);
  }

  // FORMATAÇÃO DE MENSAGEM DE ERRO CASO O CPF DO USUÁRIO SEJA INVALIDO
  const ErrorValidationLabel = ({ txtLbl }) => (
    <label htmlFor="" style={{ color: "red" }}>
      {txtLbl}
    </label>
  );
  // HTML(JSX) DA PÁGINA  
  return (
    <>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <h2>Assinador BRy PDF (KMS)</h2>
      {/* FORMULÁRIO PARA PEGAR OS DADOS DO USUÁRIO */}
      <form onSubmit={handleSubmit}>
        <label htmlFor="tipo_credencial">Tipo da Credencial *</label>
        <select
          name="tipo_credencial"
          value={tipo_credencial}
          required
          onChange={event => setTipoCredencialFunc(event.target.value)}
        >
          <option value="PIN">PIN</option>
          <option value="TOKEN">TOKEN</option>
          <option value="GOVBR">GOVBR</option>
        </select>

        <label htmlFor="valor_credencial">Valor da Credencial *</label>
          <input
            id="valor_credencial"
            required
            placeholder="Valor da Credencial"
            value={valor_credencial}
            type={tipo_credencial === "PIN" ? ("password") : ("text")}
            onChange={event => setValorCredencial(event.target.value)}
          />

        <label htmlFor="docLabel">Documento a ser assinado *</label>
        <label htmlFor="documento" className="fileUp">
          {documento ? (
            <React.Fragment>
              {documento.name}
            </React.Fragment>
          ) : (
              <React.Fragment>
                <i className="fa fa-upload"></i>
                Selecione o arquivo
              </React.Fragment>
            )}
          <input
            id="documento"
            type="file"
            accept=".pdf"
            required
            onChange={event => setDocumento(event.target.files[0])}
          />
        </label>
        
        {tipo_credencial === "GOVBR" ? (<></>) : 
        (<>
          <label htmlFor="cpf">CPF</label>
          <input
            id="cpf"
            type="number"
            placeholder="CPF do Assinante"
            value={cpf}
            onChange={event => setCpf(event.target.value)}
          />
          {cpfInvalido ? <ErrorValidationLabel txtLbl="CPF Invalido" /> : ""}
        </>)}

        <label htmlFor="perfil">Perfil de Assinatura *</label>
        <select
          name="perfil"
          value={perfil}
          onChange={event => setPerfil(event.target.value)}
        >
          <option value="BASICA">Basica</option>
          <option value="CARIMBO">Com carimbo do tempo</option>
        </select>

        <label htmlFor="algoritmoHash">Algoritmo Hash *</label>
        <select
          name="algoritmoHash"
          value={algoritmoHash}
          onChange={event => setAlgoritmoHash(event.target.value)}
        >
          <option value="SHA1">SHA1</option>
          <option value="SHA256">SHA256</option>
          <option value="SHA512">SHA512</option>
         </select>

        <label htmlFor="assinaturaVisivel">Assinatura visível? *</label>
        <select
          name="assinaturaVisivel"
          value={assinaturaVisivel}
          onChange={event => setAssinaturaVisivel(event.target.value)}
        >
          <option value="false" >Não</option>
          <option value="true" >Sim</option>
        </select>

        {assinaturaVisivel === "true" ? (
              <>
               <label htmlFor="altura">Altura do campo de assinatura *</label>
                <input
                  id="altura"
                  type="number"
                  required
                  placeholder="Altura do campo de assinatura"
                  value={altura}
                  onChange={event => setAltura(event.target.value)}
                />

                <label htmlFor="largura">Largura do campo de assinatura *</label>
                <input
                  id="largura"
                  type="number"
                  required
                  placeholder="Largura do campo de assinatura"
                  value={largura}
                  onChange={event => setLargura(event.target.value)}
                />

                <label htmlFor="coordenadaX">Coordenada X *</label>
                <input
                  id="coordenadaX"
                  type="number"
                  required
                  placeholder="Coordenada X do campo de assinatura"
                  value={coordenadaX}
                  onChange={event => setCoordenadaX(event.target.value)}
                />

                <label htmlFor="coordenadaY">Coordenada Y *</label>
                <input
                  id="coordenadaY"
                  type="number"
                  required
                  placeholder="Coordenada Y do campo de assinatura"
                  value={coordenadaY}
                  onChange={event => setCoordenadaY(event.target.value)}
                />

                <label htmlFor="posicao">Posicao do campo de assinatura *</label>
                <select
                  name="posicao"
                  value={posicao}
                  onChange={event => setPosicao(event.target.value)}
                >
                  <option value="INFERIOR_ESQUERDO">Inferior Esquerdo</option>
                  <option value="INFERIOR_DIREITO">Inferior Direito</option>
                  <option value="SUPERIOR_ESQUERDO">Superior Esquerdo</option>
                  <option value="SUPERIOR_DIREITO">Superior Direito</option>
                </select>

                <label htmlFor="PAGINA">Página do campo de assinatura *</label>
                <select
                  name="pagina"
                  value={pagina}
                  onChange={event => setPagina(event.target.value)}
                >
                  <option value="PRIMEIRA">Primeira</option>
                  <option value="ULTIMA">Última</option>
                  <option value="TODAS">Todas</option>
                </select>

                <label htmlFor="incluirIMG">Incluir imagem? *</label>
            <select
              name="incluirIMG"
              value={incluirIMG}
              onChange={event => setIncluirIMG(event.target.value)}
            >
              <option value="false" >Não</option>
              <option value="true" >Sim</option>
            </select>

            {incluirIMG === "true" ? (
              <>
                <h2>Configurações de imagem</h2>

                <label htmlFor="lblImagem">Imagem *</label>

                <label htmlFor="imagem" className="fileUp">
                  {imagem ? (
                    <React.Fragment>{imagem.name}</React.Fragment>
                  ) : (
                      <React.Fragment>
                        <i className="fa fa-upload"></i>
                      Selecione a imagem de assinatura
                      </React.Fragment>
                    )}

                  <input
                    id="imagem"
                    type="file"
                    accept=".png, .jpeg, .jpg, .bmp"
                    required
                    onChange={event => setImagem(event.target.files[0])}
                  />
                </label>
               
              </>
            ) : ("")}
                <label htmlFor="incluirTXT">Incluir texto? *</label>
                <select
                  name="incluirTXT"
                  value={incluirTXT}
                  onChange={event => setIncluirTXT(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>

                {incluirTXT === "true" ? (
                  <>
                    <label htmlFor="texto">Texto de Assinatura*</label>
                    <input
                      id="texto"
                      type="text"
                      required
                      placeholder="Texto incluso na Assinatura"
                      value={texto}
                      onChange={event => setTexto(event.target.value)}
                      />

                  </>
                ) : (
                  ""
                )}

                <label htmlFor="incluirCPF">Incluir CPF na Assinatura?</label>
                <select
                  name="incluirCPF"
                  value={incluirCPF}
                  onChange={event => setIncluirCPF(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>

                <label htmlFor="incluirEmail">Incluir Email na Assinatura?</label>
                <select
                  name="incluirEmail"
                  value={incluirEmail}
                  onChange={event => setIncluirEmail(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>

                <label htmlFor="incluirCN">Incluir Nome na Assinatura?</label>
                <select
                  name="incluirCN"
                  value={incluirCN}
                  onChange={event => setIncluirCN(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>
                  </>
                ) : ("")}

              <label htmlFor="tipoDocumento">- - Inclusão de Metadados em Prescrição Eletrônica - -</label>
              <label htmlFor="tipoDocumento">Tipo de Documento *</label>
              <select
                  name="tipoDocumento"
                  value={tipoDocumento}
                  onChange={event => setTipoDocumento(event.target.value)}
              >
                  <option value="">Selecione uma opção...</option>
                  <option value="2.16.76.1.12.1.1">Prescrição de medicamento</option>
                  <option value="2.16.76.1.12.1.2">Atestado médico</option>
                  <option value="2.16.76.1.12.1.3">Solicitação de exame</option>
                  <option value="2.16.76.1.12.1.4">Laudo laboratorial</option>
                  <option value="2.16.76.1.12.1.5">Sumária de alta</option>
                  <option value="2.16.76.1.12.1.6">Registro de atendimento clinico</option>
                  <option value="2.16.76.1.12.1.7">Dispensação de medicamentos</option>
                  <option value="2.16.76.1.12.1.8">Vacinação</option>
                  <option value="2.16.76.1.12.1.11">Relatório médico</option>
              </select>

              <label htmlFor="tipoProfissional">Tipo de Profissional*</label>
              <select
                  name="tipoProfissional"
                  value={tipoProfissional}
                  onChange={event => setTipoProfissional(event.target.value)}
              >
                  <option value="">Selecione uma opção...</option>
                  <option value="MEDICO">Médico</option>
                  <option value="FARMACEUTICO">Farmacêutico</option>
                  <option value="ODONTOLOGISTA">Odontologista</option>
              </select>

              <label htmlFor="cpf">Número no respectivo conselho *</label>
              <input
                  id="numero"
                  type="number"
                  required
                  placeholder="Número no Conselho"
                  value={numero}
                  onChange={event => setNumero(event.target.value)}
              />

              <label htmlFor="cpf">UF do Profissional *</label>
              <input
                  id="UF"
                  type="text"
                  required
                  placeholder="Estado"
                  value={UF}
                  onChange={event => setUF(event.target.value)}
              />

              <label htmlFor="cpf">Especialidade *</label>
              <input
                  id="especialidade"
                  type="text"
                  required
                  placeholder="Ex: Cardiologista"
                  value={especialidade}
                  onChange={event => setEspecialidade(event.target.value)}
              />

        <button className="btn" type="submit">
          Assinar
        </button>

        <label>
          {loading ? "Realizando a assinatura do documento..." : ""}
        </label>
      </form>
    </>
  );
}
