# Frontend ReactJS de Geração de Assinatura PDF com certificado em nuvem.

Este exemplo apresenta os passos necessários para a geração de assinatura PDF utilizando certificado digital instalado em nuvem.

### Formas de acesso ao Certificado em Nuvem

* *BRy KMS*: Certificado está armazenado na Plataforma de Certificados em Nuvem da BRy Tecnologia. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **PIN** ou **TOKEN**. Para credenciais com tipo **PIN** o Valor da Credencial corresponde a senha de acesso ao certificado em nuvem, enquanto que para credenciais do tipo **TOKEN** o Valor da Credencial corresponde a um token de autorização do acesso ao certificado. Mais informações sobre o serviço de certificados em nuvem podem ser encontradas em nossa documentação da [API de Certificado em Nuvem](https://api-assinatura.bry.com.br/api-certificado-em-nuvem).

* *GovBR*: Certificado está armazenado na Plataforma de Assinatura Digital gov.br. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **GOVBR** e o Valor da Credencial corresponde ao token de acesso do usuário. É possível verificar um exemplo da geração deste token de acesso em nossa [API Autenticação GOV.BR](https://gitlab.com/brytecnologia-team/integracao/api-autenticacao-token-govbr/react/gestao-credenciais).

### Backend

Este exemplo frontend é compatível com os backends:

* [Python](https://gitlab.com/brytecnologia-team/integracao/api-assinatura-medica/python/geracao-de-assinatura-medica-de-prescricao-eletronica)
* [PHP](https://gitlab.com/brytecnologia-team/integracao/api-assinatura-medica/php/geracao-de-assinatura-medica-de-prescricao-eletronica)


### Tech

O exemplo utiliza das bibliotecas JavaScript abaixo:
* [ReactJS] - A JavaScript library for building user interfaces!
* [Axios] - Promise based HTTP client for the browser and node.js.

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. 
Utilizamos o ReactJS versão 16.10 para desenvolvimento e o [Npm] versão 6.13 ou o [Yarn] versão 1.22 para instalação das dependências e execução da aplicação.

##### Comandos:

**Instalar as dependências utilizando o comando abaixo:**

    - npm install

  ou

    - yarn

**Executar programa:**

    - npm start

  ou

    - yarn start

   [Node]: <https://nodejs.org/en/>
   [ReactJS]: <https://reactjs.org/>
   [Axios]: <https://github.com/axios/axios>
   [Yarn]: <https://yarnpkg.com/>
   [Npm]: <https://www.npmjs.com/>
